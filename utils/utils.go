package utils

import (
	"hash/fnv"
	"regexp"
)

var (
	rpEmail = regexp.MustCompile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")
	rpName  = regexp.MustCompile("^[_A-Za-z0-9-]{4,12}$")
)

// Hash makes a unique hash number of a string to get a unique
// number as the user's ID
func Hash(s string) int {
	h := fnv.New32a()
	h.Write([]byte(s))
	return int(h.Sum32())
}

// IsValidEmail checks whether the email is valid or not
func IsValidEmail(email string) bool {
	return rpEmail.MatchString(email)
}

// IsValidName checks wether the name is valid or not
// A new name is valid if it has between [4, 12] characters, and does not contain any special characters
func IsValidName(name string) bool {
	return rpName.MatchString(name)
}
