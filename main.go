package main

import (
	"NearTalkRest/router"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"NearTalkRest/subjects"

	"gopkg.in/mgo.v2/bson"
)

const defaultPort string = "8080"

func main() {
	router := router.NewRouter()

	session, err := subjects.InitMongo()
	if err != nil {
		panic(err)
	}
	defer session.Close()

	port := os.Getenv("PORT")
	if port == "" {
		port = defaultPort
	}

	go func() {
		t := time.Tick(5 * time.Minute)
		c := session.DB("neartalkdb").C("rooms")
		for range t {
			c.RemoveAll(bson.M{"connections": bson.M{"$lte": 0}})
		}
	}()

	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c, os.Interrupt, syscall.SIGTERM)
		<-c
		session.Close()
		fmt.Println("\nServer closed")
		os.Exit(1)
	}()

	log.Fatal(http.ListenAndServe(":"+port, router))
}
