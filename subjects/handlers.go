package subjects

import (
	"NearTalkRest/utils"
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"strconv"
	"time"

	"strings"

	"fmt"

	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

var db *mgo.Database

const offset int = 100

type coords struct {
	Position [2]float32 `json:"position" bson:"position"`
}

func (c coords) String() string {
	return fmt.Sprintf("[%v, %v]", c.Position[0], c.Position[1])
}

type username struct {
	Name string `json:"name" bson:"name"`
}

// InitMongo creates a mgo.Session (which is a connection to the database),
// to be able to make requests to all the collectionss
func InitMongo() (*mgo.Session, error) {
	session, err := mgo.Dial("mongodb://adminNT:adminNT@ds111461.mlab.com:11461/neartalkdb")
	db = session.DB("neartalkdb")
	return session, err
}

// GetUsers prints an array of all the users
func GetUsers(w http.ResponseWriter, r *http.Request) {
	m := make(map[string]interface{})

	q := r.URL.Query()
	if val := q["name"]; len(val) != 0 {
		m["name"] = val[0]
	}

	c := db.C("users")
	result := []User{}

	tmp := []User{}
	skip := 0
	for len(result)%offset == 0 || skip == 0 {
		c.Find(m).Skip(skip).Limit(offset).All(&tmp)
		if len(tmp) == 0 {
			break
		}
		result = append(result, tmp...)
		skip += offset
	}

	if err := json.NewEncoder(w).Encode(result); err != nil {
		log.Println("GetUsers() | Error encoding result: ", err)
		return
	}
}

// GetRooms prints an array of all the rooms
func GetRooms(w http.ResponseWriter, r *http.Request) {
	c := db.C("rooms")
	result := []Room{}

	tmp := []Room{}
	skip := 0
	for len(result)%offset == 0 || skip == 0 {
		c.Find(nil).Skip(skip).Limit(offset).All(&tmp)
		if len(tmp) == 0 {
			break
		}
		result = append(result, tmp...)
		skip += offset
	}

	if err := json.NewEncoder(w).Encode(result); err != nil {
		log.Println("GetRooms() | Could not encode: ", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

// GetUserRoom prints the users current room
func GetUserRoom(w http.ResponseWriter, r *http.Request) {
	id := mux.Vars(r)["id"]
	if id == "" {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	if !existsInUsers("ntid", id) {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	usersCol := db.C("users")
	user := User{}
	usersCol.Find(bson.M{"ntid": id}).One(&user)

	roomsCol := db.C("rooms")

	room := Room{}
	if user.Room != "" {
		roomsCol.Find(bson.M{"ntzoneid": user.Room}).One(&room)
		if err := json.NewEncoder(w).Encode(room); err != nil {
			log.Println("GetUserRoom() | Could not encode: ", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		return
	}

	roomsCol.Find(bson.M{
		"location": bson.M{
			"$nearSphere": bson.M{
				"$geometry": bson.M{
					"type":        "Point",
					"coordinates": user.Position,
				},
				"$maxDistance": 500,
			},
		},
	}).One(&room)

	if room.ID == "" {
		rand.Seed(time.Now().UTC().UnixNano())
		room.ID = strconv.Itoa(rand.Int())
		room.Location.Coordinates = user.Position
		room.Location.Type = "Point"
		roomsCol.Insert(room)
	}
	usersCol.Update(bson.M{"ntid": id}, bson.M{"$set": bson.M{"room": room.ID}})
	user.Room = room.ID

	if err := json.NewEncoder(w).Encode(room); err != nil {
		log.Println("PostUser() | Could not encode: ", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

// PostUser creates a new user
func PostUser(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
	if err != nil {
		log.Println("PostUser() | Error reading from body: ", err)
		w.WriteHeader(http.StatusNoContent)
		return
	}
	if err := r.Body.Close(); err != nil {
		log.Println("PostUser() | Error closing body: ", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	var user User
	if err := json.Unmarshal(body, &user); err != nil {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusUnprocessableEntity)
		if err := json.NewEncoder(w).Encode(err); err != nil {
			log.Println("PostUser() | Error encoding: ", err)
			w.WriteHeader(http.StatusInternalServerError)
		}
		return
	}

	if user.Email == "" || !utils.IsValidEmail(user.Email) {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	if existsInUsers("email", user.Email) {
		wrong := &Error{Err: "598234679"}
		w.WriteHeader(http.StatusConflict)
		if err := json.NewEncoder(w).Encode(wrong); err != nil {
			log.Println("PostUser() | Error encoding: ", err)
			w.WriteHeader(http.StatusInternalServerError)
		}
		return
	}
	if user.Name == "" || !utils.IsValidName(user.Name) {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	if existsInUsers("name", user.Name) {
		wrong := &Error{Err: "524532455"}
		w.WriteHeader(http.StatusConflict)
		if err := json.NewEncoder(w).Encode(wrong); err != nil {
			log.Println("PostUser() | Error encoding: ", err)
			w.WriteHeader(http.StatusInternalServerError)
		}
		return
	}

	user.Email = strings.ToLower(user.Email)
	user.ID = strconv.Itoa(utils.Hash(user.Email))
	user.Room = ""
	user.Position = [2]float32{}
	fmt.Printf("New user is: %v\n", user)

	c := db.C("users")
	if err := c.Insert(&user); err != nil {
		log.Println("PostUser() | Error inserting a new user: ", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	client := &Client{ID: user.ID, Name: user.Name}
	if err := json.NewEncoder(w).Encode(client); err != nil {
		log.Printf("PostUser() | Could not encode: '%v' | Error: %v\n", client, err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

// DelUser deletes an user, specified by the id in the url .../users/{id}
func DelUser(w http.ResponseWriter, r *http.Request) {
	id := mux.Vars(r)["id"]
	if id == "" {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	if !existsInUsers("ntid", id) {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	usersCol := db.C("users")
	user := User{}
	if err := usersCol.Find(bson.M{"ntid": id}).One(&user); err != nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	roomsCol := db.C("rooms")
	var room Room
	if err := roomsCol.Find(bson.M{"ntzoneid": user.Room}).One(&room); err != nil || room.ID == "" {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	if err := roomsCol.Update(bson.M{"ntzoneid": user.Room}, bson.M{"$inc": bson.M{"connections": -1}}); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if err := usersCol.Remove(bson.M{"ntid": id}); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

// UpdateUserCoords updates the user's position, and prints a bit of information about its current room
func UpdateUserCoords(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
	if err != nil {
		log.Println("UpdateUserCoords() | Error reading from body: ", err)
		w.WriteHeader(http.StatusNoContent)
		return
	}
	if err := r.Body.Close(); err != nil {
		log.Println("UpdateUserCoords() | Error closing body: ", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	var coords coords
	if err := json.Unmarshal(body, &coords); err != nil {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusUnprocessableEntity)
		if err := json.NewEncoder(w).Encode(err); err != nil {
			log.Println("UpdateUserCoords() | Error encoding: ", err)
			w.WriteHeader(http.StatusInternalServerError)
		}
		log.Println("UpdateUserCoords() |", err)
		return
	}

	id := mux.Vars(r)["id"]
	if id == "" { //Shoud not happen
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	if !existsInUsers("ntid", id) {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	usersCol := db.C("users")
	usersCol.Update(bson.M{"ntid": id}, bson.M{"$set": bson.M{"position": coords.Position}})
	fmt.Printf("{user.ID: %v, user.coords: %v}\n", id, coords)

	user := User{}
	usersCol.Find(bson.M{"ntid": id}).One(&user)

	if user.ID == "" {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	room := getUserRoom(user.Position)
	if room.ID != user.Room {
		usersCol.Update(bson.M{"ntid": id}, bson.M{"$set": bson.M{"room": room.ID}})
		roomsCol := db.C("rooms")
		roomsCol.Update(bson.M{"ntzoneid": user.Room}, bson.M{"$inc": bson.M{"connections": -1}})
		user.Room = room.ID
		roomsCol.Update(bson.M{"ntzoneid": room.ID}, bson.M{"$inc": bson.M{"connections": 1}})
	}

	type customRoom struct {
		ID       string   `json:"ntzoneid"`
		Location GeoJSON  `json:"location"`
		Users    []string `json:"users"`
	}
	customizedRoom := customRoom{}

	customizedRoom.ID = room.ID
	customizedRoom.Location = room.Location
	customizedRoom.Users = getRoomUsers(room.ID)

	if err = json.NewEncoder(w).Encode(customizedRoom); err != nil {
		log.Println("UpdateUserCoords() | Eror encoding: ", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

// GetRoomUsers prints the rooms users names
func GetRoomUsers(w http.ResponseWriter, r *http.Request) {
	id := mux.Vars(r)["id"]
	if id == "" { //Should not happen
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	if !roomExists(id) {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	var usernames struct {
		Names []string `json:"names" bson:"names"`
	}
	usernames.Names = getRoomUsers(id)

	if err := json.NewEncoder(w).Encode(usernames); err != nil {
		log.Println("GetRoomUsers() | Could not encode: ", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func getRoomUsers(id string) []string {
	c := db.C("users")
	users := []username{}
	tmp := []username{}
	skip := 0
	for len(users)%offset == 0 || skip == 0 {
		c.Find(bson.M{"room": id}).Skip(skip).Limit(offset).All(&tmp)
		if len(tmp) == 0 {
			break
		}
		users = append(users, tmp...)
		skip = skip + offset
	}

	names := []string{}
	for _, name := range users {
		names = append(names, name.Name)
	}
	return names
}

// CheckCorrectLogin checks whether the email passed as parameter is a valid email (exists in our users collection)
// And if it is so, returns the needed information, an id and its name
func CheckCorrectLogin(w http.ResponseWriter, r *http.Request) {
	email := r.URL.Query().Get("email")
	if email == "" {
		w.WriteHeader(http.StatusNoContent)
		return
	}

	c := db.C("users")
	var user User
	if err := c.Find(bson.M{"email": email}).One(&user); err != nil || user.ID == "" {
		log.Printf("PostUser() | Cannot find user with email: '%v'\n", email)
		w.WriteHeader(http.StatusNotFound)
		return
	}

	client := &Client{ID: user.ID, Name: user.Name}
	if err := json.NewEncoder(w).Encode(client); err != nil {
		log.Printf("PostUser() | Could not encode: '%v' | Error: %v\n", client, err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

// ChangeUserName checks if the new username is valid and available and updates the users name,
// if it is not, returns an error
func ChangeUserName(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
	if err != nil {
		log.Println("ChangeUserName() | Error reading from body: ", err)
		w.WriteHeader(http.StatusNoContent)
		return
	}
	if err := r.Body.Close(); err != nil {
		log.Println("ChangeUserName() | Error closing body: ", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	var username username
	if err := json.Unmarshal(body, &username); err != nil {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusUnprocessableEntity)
		if err := json.NewEncoder(w).Encode(err); err != nil {
			log.Println("ChangeUserName() | Error encoding: ", err)
			w.WriteHeader(http.StatusInternalServerError)
		}
		log.Println("ChangeUserName() |", err)
		return
	}
	id := mux.Vars(r)["id"]
	if id == "" { //Shoud not happen
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	if !existsInUsers("ntid", id) {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	if username.Name == "" || !utils.IsValidName(username.Name) {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	if existsInUsers("name", username.Name) {
		wrong := &Error{Err: "524532455"}
		w.WriteHeader(http.StatusConflict)
		if err := json.NewEncoder(w).Encode(wrong); err != nil {
			log.Println("ChangeUserName() | Error encoding: ", err)
			w.WriteHeader(http.StatusInternalServerError)
		}
		return
	}

	c := db.C("users")
	if err := c.Update(bson.M{"ntid": id}, bson.M{"$set": bson.M{"name": username.Name}}); err != nil {
		log.Printf("ChangeUserName() | Could not update user[%v]: %v\n", id, err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

// DisconnectUser tells the databse that this user is no longer using the app
// and that it should be disconnected from its room
func DisconnectUser(w http.ResponseWriter, r *http.Request) {
	id := mux.Vars(r)["id"]
	if id == "" { //Shoud not happen
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	if !existsInUsers("ntid", id) {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	usersCol := db.C("users")
	var user User
	if err := usersCol.Find(bson.M{"ntid": id}).One(&user); err != nil || user.ID == "" {
		log.Printf("DisconnectUser() | Cannot find user with id: '%v'\n", id)
		w.WriteHeader(http.StatusNotFound)
		return
	}
	if user.Room == "" {
		log.Println("DisconnectUser() | Disconnecting a disconnected user")
		return
	}

	roomsCol := db.C("rooms")
	if err := roomsCol.Update(bson.M{"ntzoneid": user.Room}, bson.M{"$inc": bson.M{"connections": -1}}); err != nil {
		log.Printf("DisconnectUser() | Cannot update room connections: '%v'\n", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if err := usersCol.Update(bson.M{"ntid": user.ID}, bson.M{"$set": bson.M{"room": ""}}); err != nil {
		log.Printf("DisconnectUser() | Cannot update reset user room: '%v'\n", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

// GetUser prints the requested user to the output
func GetUser(w http.ResponseWriter, r *http.Request) {
	id := mux.Vars(r)["id"]
	if id == "" {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	if !existsInUsers("ntid", id) {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	c := db.C("users")
	var user User
	if err := c.Find(bson.M{"ntid": id}).One(&user); err != nil || user.ID == "" {
		log.Printf("GetUser() | Cannot find user with id: '%v'\n", id)
		w.WriteHeader(http.StatusNotFound)
		return
	}
	if err := json.NewEncoder(w).Encode(user); err != nil {
		log.Printf("GetUser() | Could not encode: '%v' | Error: %v\n", user, err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

// GetRoom print the requested room to the output
func GetRoom(w http.ResponseWriter, r *http.Request) {
	id := mux.Vars(r)["id"]
	if id == "" {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	if !roomExists(id) {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	c := db.C("rooms")
	var room Room
	if err := c.Find(bson.M{"ntzoneid": id}).One(&room); err != nil || room.ID == "" {
		log.Printf("GetRoom() | Cannot find room with id: '%v'\n", id)
		w.WriteHeader(http.StatusNotFound)
		return
	}
	if err := json.NewEncoder(w).Encode(room); err != nil {
		log.Printf("GetRoom() | Could not encode: '%v' | Error: %v\n", room, err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func existsInUsers(field, value string) bool {
	c := db.C("users")
	var user User
	c.Find(bson.M{field: bson.M{"$regex": bson.RegEx{Pattern: fmt.Sprintf("^%v$", value), Options: "i"}}}).One(&user)
	return user.ID != ""
}

func roomExists(id string) bool {
	c := db.C("rooms")
	var room Room
	c.Find(bson.M{"ntzoneid": bson.M{"$regex": bson.RegEx{Pattern: fmt.Sprintf("^%v$", id), Options: "i"}}}).One(&room)
	return room.ID != ""
}

func getUserRoom(position [2]float32) Room {
	//db.rooms.createIndex({location:"2dsphere"});

	c := db.C("rooms")
	room := Room{}
	c.Find(bson.M{
		"location": bson.M{
			"$nearSphere": bson.M{
				"$geometry": bson.M{
					"type":        "Point",
					"coordinates": position,
				},
				"$maxDistance": 500,
			},
		},
	}).One(&room)

	if room.ID == "" {
		room.ID = getRoomID()
		room.Location.Coordinates = position
		room.Location.Type = "Point"
		room.Connections = 0
		c.Insert(room)
	}

	return room
}

func getRoomID() string {
	room := Room{}
	c := db.C("rooms")
	rand.Seed(time.Now().UTC().UnixNano())
	id := strconv.Itoa(rand.Int())
	c.Find(bson.M{"ntzoneid": id}).One(&room)
	for roomExists(room.ID) {
		id = strconv.Itoa(rand.Int())
		c.Find(bson.M{"ntzoneid": id}).One(&room)
	}
	return id
}
