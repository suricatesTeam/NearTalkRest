package subjects

import (
	"fmt"
)

// GeoJSON is helper struct to make searches in mongo based on a position
type GeoJSON struct {
	Type        string     `json:"-"`
	Coordinates [2]float32 `json:"coordinates"`
}

// Room the zone where the users talks to
type Room struct {
	ID          string  `json:"ntzoneid" bson:"ntzoneid"`
	Location    GeoJSON `json:"location" bson:"location"`
	Connections int     `json:"connections" bson:"connections"`
}

func (r Room) String() {
	fmt.Printf("ID: %v\nPosition: %v\nConnections: %d", r.ID, r.Location.Coordinates, r.Connections)
}

// User a struct to acces the users information easily
type User struct {
	ID       string     `json:"ntid" bson:"ntid"`
	Name     string     `json:"name" bson:"name"`
	Email    string     `json:"email" bson:"email"`
	Position [2]float32 `json:"position" bson:"position"`
	Room     string     `json:"room" bson:"room"`
}

func (u User) String() string {
	return fmt.Sprintf("{ID: %v, Name: %v, Email: %v, Position: [%v, %v], Room: %v}", u.ID, u.Name, u.Email, u.Position[0], u.Position[1], u.Room)
}

// Client a struct to return just a piece of information about the user
type Client struct {
	ID   string `json:"ntid"`
	Name string `json:"name"`
}

// Error a custom error
type Error struct {
	Err string `json:"error"`
}
