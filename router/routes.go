package router

import (
	subjectsPck "NearTalkRest/subjects"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

type route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type routes []route

var allRoutes = routes{
	route{
		"Home",
		"Get",
		"/",
		home,
	},
	route{
		"GetUsers",
		"Get",
		"/users",
		subjectsPck.GetUsers,
	},
	route{
		"GetRooms",
		"Get",
		"/rooms",
		subjectsPck.GetRooms,
	},
	route{
		"PostUser",
		"POST",
		"/users",
		subjectsPck.PostUser,
	},
	route{
		"GetUser",
		"Get",
		"/users/{id}",
		subjectsPck.GetUser,
	},
	route{
		"DelUser",
		"DELETE",
		"/users/{id}",
		subjectsPck.DelUser,
	},
	route{
		"GetUserRoom",
		"Get",
		"/users/{id}/room",
		subjectsPck.GetUserRoom,
	},
	route{
		"UpdateUserCoords",
		"Put",
		"/users/{id}/coords",
		subjectsPck.UpdateUserCoords,
	},
	route{
		"ChangeUserName",
		"Put",
		"/users/{id}/name",
		subjectsPck.ChangeUserName,
	},
	route{
		"DisconnectUser",
		"Put",
		"/users/{id}/disconnect",
		subjectsPck.DisconnectUser,
	},
	route{
		"GetRoom",
		"Get",
		"/rooms/{id}",
		subjectsPck.GetRoom,
	},
	route{
		"GetRoomUsers",
		"Get",
		"/rooms/{id}/users",
		subjectsPck.GetRoomUsers,
	},
	route{
		"LoginUser",
		"Get",
		"/login",
		subjectsPck.CheckCorrectLogin,
	},
}

// NewRouter create a router with all of the routes of the api
//
// A route cointains: A name, the method type, the url where the client access
// and the method to do when the url is accessed
func NewRouter() *mux.Router {
	router := mux.NewRouter().StrictSlash(true)
	for _, route := range allRoutes {
		router.
			Methods(route.Method).
			Path(route.Pattern).
			Name(route.Name).
			Handler(route.HandlerFunc)
	}

	return router
}

func home(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "<h1>Welcome to Near Talk REST API</h1>")
}
